<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
<div class="row"> 
    <div class="col-100">
    <br><br>
        <a href="/home"><button class="btn btn-primary active">Home</button></a>
        <a href="/home/create"><button class="btn btn-success">Create Record</button></a>
        <a href="/home/trashed"><button class="btn btn-info">Trashed Record</button></a>
        <a href="/home/users"><button class="btn btn-info">Users Record</button></a>
        <a href="/home/createuser"><button class="btn btn-basic">Create User</button></a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST">
                    @csrf
                    <input type="submit" class="btn btn-danger" style="float:right;" value=" {{ Auth::user()->name }}&nbsp;&nbsp;&nbsp;Logout">
        </form>
    </div>
</div>
  <h2 align="center">All Records</h2>         
  <table class="table">
    <thead>
      <tr>
        <th>Name</th>
        <th>Description</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    @foreach($flight as $res)
      <tr>
        <td>{{$res->name}}</td>
        <td>{{$res->description}}</td>
        <td><a href="/home/delete/{{$res->id}}"><button class="btn btn-danger">Delete</button></a></td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

</body>
</html>
