<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Flight;
use App\User;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class FlightController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $flight = Flight::all();
        return view('home')->with('flight',$flight);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function delete($id)
    {
        $output=Flight::find($id);
        $output->delete();
        return redirect('/home');
    }
    public function deleteUser($id)
    {
        $output=User::find($id);
        $output->delete();
        return redirect('/home');
    }
    public function restore($id)
    {
        $output=Flight::withTrashed()->find($id);
        $output->restore();
        return redirect('/home');
    }
    public function trash()
    {
                $flight=DB::table('flights')->whereNotNull('deleted_at')->get();
                return view('trash')->with('flight',$flight);
    }
    public function user()
    {
                $users=User::all();
                return view('user')->with('users',$users);
    }
    public function createUser()
    {
                return view('register');
    }
    
}
