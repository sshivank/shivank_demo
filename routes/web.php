<?php
use App\Flight;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});
Route::get('/home','FlightController@index');
Route::get('/home/delete/{id}','FlightController@delete');
Route::get('/home/deleteuser/{id}','FlightController@deleteUser');
Route::get('/home/restore/{id}','FlightController@restore');
Route::get('/home/trashed','FlightController@trash');
Route::get('/home/users','FlightController@user');
Route::get('/home/create','FlightController@create');
Route::get('/home/createuser','FlightController@createUser');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
